const express = require('express');

//apps
const app1 = express();
const app2 = express();

//routes
app1.get('/users', (req, res) => {
    res.send('hello from App 1');
})

app1.get('db/users', (req, res) => {
    res.send('hello from App 1');
})

app2.get('/users', (req, res) => {
    res.send('hello from App 2');
})


//listen

//serving my html files
app1.listen(80, () => {

    console.log('App 1 is listening on port 6001');
})


//serving my api
app2.listen(6002, () => {
    console.log('App 2 is listening on port 6002');
})

//serving my DB
app2.listen(6003, () => {
    console.log('App 2 is listening on port 6003');
})