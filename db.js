const fs = require('fs');
const { nanoid } = require('nanoid');

const path = `${__dirname}/db`;

class DB {
    constructor(dbName) {
        this.dbName = dbName;
        this.dbPath = `${path}/${dbName}.json`
    }

    create = () => {
        this.save([])
        console.log(`${this.dbName} Created!`);
    }

    save = (data) => {
        fs.writeFileSync(this.dbPath, JSON.stringify(data));
    }

    get = () => {
        const data = fs.readFileSync(this.dbPath, 'utf-8');
        return JSON.parse(data);
    }

    add = (item) => {
        //how can I add new Item to the users.json list ?
        //1) get the data
        //2) push new item to the data
        //3) save the entire new data

        const data = this.get();
        const newId = nanoid();

        data.push({ ...item, id: newId });
        this.save(data);

        return newId;
    }

    getById = (id) => {
        //should return the item
        const data = this.get();
        const item = data.find(i => i.id === id);
        return item;
    }

    find = (query) => {
        //should return the item
        const data = this.get();
        const item = data.find(i => query(i));
        return item;
    }

    update = (id, item) => {
        //users.update('nIGH6-jm_O97gs_qiw6-p', { name: 'mosh', age: 25 })
        const data = this.get();
        const index = data.findIndex(i => i.id == id);
        const curr = data[index];
        data[index] = { ...item, id: curr.id };

        this.save(data);
    }

    updateCol = (id, key, value) => {
        //users.updateCol('nIGH6-jm_O97gs_qiw6-p', 'name', 'hello');
        const data = this.get();
        const index = data.findIndex(i => i.id === id);

        data[index][key] = value;

        this.save(data);
    }

    del = (id) => {
        const data = this.get();
        const newData = data.filter(i => i.id !== id);
        this.save(newData);
    }
}



module.exports = DB;



