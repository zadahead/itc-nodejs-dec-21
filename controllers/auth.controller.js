
const usersService = require('../services/users.service');
const authService = require('../services/auth.service');

const jwtLib = require('../lib/jwt.lib');
const ErrorHandler = require('../lib/errorHandling.lib');

const register = (req, res, next) => {
    //clean
    const user = { ...req.body };
    delete user.repassword;

    //find if a user exist
    if (usersService.findByEmail(user.email)) {
        return next('user already exist');
    }

    //create a hash
    const hash = authService.generateHash(req.body.password);

    //add new user
    const newId = usersService.add({ ...user, password: hash });

    //return
    res.send({
        success: true,
        newId
    });
}

const login = (req, res, next) => {

    const { email, password } = req.body;

    const user = usersService.findByEmail(email);

    if (!user) {
        return next(ErrorHandler.userNotFound());
    }

    const isValid = authService.validateHash(password, user.password);

    delete user.password;

    if (isValid) {
        authService.saveUserInCache(user);
        const token = authService.generateToken(user.id);

        return res.send({
            user,
            isValid,
            ...token
        });
    }
    next(ErrorHandler.loginFailed());
}




module.exports = {
    register,
    login
}
