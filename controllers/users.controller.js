const permissions = require('../lib/permissions.lib');
const usersService = require('../services/users.service');

const getUsers = (req, res) => {
    const users = usersService.getAll();
    res.send(users);
}

const getMe = (req, res) => {
    console.log('req.user', req.user);

    const u = usersService.getById(req.user.userId);
    u.scopes = permissions.get(u.permissions);

    res.send(u);
}

const getAdminData = (req, res) => {
    res.send('I am an admin!');
}

const getById = (req, res) => {
    const user = usersService.getById(req.params.userId);
    res.send(user);
}

const addNew = (req, res) => {
    const newId = usersService.add(req.body);
    res.send(newId);
}

const update = (req, res) => {
    const id = req.params.userId;
    const item = req.body;

    usersService.update(id, item);
    res.send('User Updated');
}

const updateCol = (req, res) => {
    const id = req.params.userId;
    const data = req.body;

    usersService.updateCol(id, data.key, data.value);
    res.send('User Col Updated');
}

const del = (req, res) => {
    usersService.del(req.params.userId);
    res.send('User Deleted!');
}

module.exports = {
    getUsers,
    getMe,
    getById,
    addNew,
    update,
    updateCol,
    del,
    getAdminData
}