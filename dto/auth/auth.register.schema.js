const authRegisterSchema = {
    type: 'object',
    properties: {
        name: { type: 'string' },
        age: { type: 'integer' },
        email: { type: 'string', format: 'email' },
        password: { type: 'string', format: 'pass' },
        repassword: {
            type: 'string',
            const: {
                $data: '1/password'
            }
        }
    },
    required: [
        'name', 'age',
        'email', 'password', 'repassword'
    ]
}

module.exports = authRegisterSchema;