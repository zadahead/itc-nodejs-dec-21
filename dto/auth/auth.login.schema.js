const authLoginSchema = {
    type: 'object',
    properties: {
        email: { type: 'string', format: 'email' },
        password: { type: 'string', format: 'pass' }
    },
    required: [
        'email', 'password'
    ]
}


module.exports = authLoginSchema;