//init
const Ajv = require('ajv');
const addFormats = require("ajv-formats");

const ajv = new Ajv({ allErrors: true, $data: true });

addFormats(ajv);

ajv.addFormat('pass', (value) => {
    if (value.length < 6) { return false }

    return true;
})

module.exports = ajv;