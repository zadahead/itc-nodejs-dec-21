const postUserSchema = {
    type: 'object',
    properties: {
        name: { type: 'string' },
        age: { type: 'integer' }
    },
    required: ['name']
}

module.exports = postUserSchema;