const ajv = require("./ajv");

const validator = (schema) => {

    return (req, res, next) => {
        const obj = req.body;


        const validator = ajv.compile(schema);

        const valid = validator(obj);

        if (!valid) {
            return next(validator.errors);
        }

        next();
    }
}

module.exports = validator;