class ErrorHandler {
    constructor(status, msg) {
        this.status = status;
        this.msg = msg;
    }

    static needLogin = () => new ErrorHandler(401, 'You need to login');

    static userNotFound = () => new ErrorHandler(404, 'User Not Found');

    static loginFailed = () => new ErrorHandler(403, 'Login Failed');

    static notAllowed = () => new ErrorHandler(405, 'You are not allowed');

    static internalError = (msg) => new ErrorHandler(500, `Error: ${msg || 'Internal Error'}`);
}

module.exports = ErrorHandler;


