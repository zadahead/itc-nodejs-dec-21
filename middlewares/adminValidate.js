const ErrorHandler = require("../lib/errorHandling.lib");
const permissions = require("../lib/permissions.lib");

const adminMiddleware = (req, res, next) => {
    const user = req.user;

    const scopes = permissions.get(user.permissions);

    if (scopes.ADMIN) {
        return next();
    }
    return next(ErrorHandler.notAllowed());
}

module.exports = adminMiddleware;