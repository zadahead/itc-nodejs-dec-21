
const jwt = require('jsonwebtoken');
const DB = require('../db');
const ErrorHandler = require('../lib/errorHandling.lib');
const sceretJwt = 'my_own_text';

const authValidate = (req, res, next) => {
    console.log('authValidate', req.url);

    if (['/auth/login', '/auth/register', '/auth/refresh'].includes(req.url)) {
        return next();
    }
    const token = req.headers.authorization;
    console.log('access_token', token);

    jwt.verify(token, sceretJwt, (err, decoded) => {
        console.log('verify', err);

        if (err) {
            switch (err.message) {
                case 'jwt expired': return next('token expired')
                default: return next(ErrorHandler.needLogin())
            }
        }
        console.log('verify', decoded.data);

        if (decoded.data.refresh) {
            return next(ErrorHandler.needLogin());
        }

        const data = decoded.data;

        if (!data) {
            return next(ErrorHandler.needLogin());
        }

        console.log('userId', data.userId);

        const cache = new DB('redis');
        const cachedUser = cache.find((i) => i.userId === data.userId);

        req.user = {
            userId: cachedUser.userId,
            permissions: cachedUser.permissions
        }
        next()
    })
}

module.exports = authValidate;