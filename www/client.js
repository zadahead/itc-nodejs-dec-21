const fs = require('fs');
const express = require('express');
const app = express();

const PORT = 3002;

app.use(express.static(`${__dirname}/public`));


app.get('/*', (req, res) => {
    console.log('hello from server');
    res.sendFile(`${__dirname}/index.html`);
})



app.listen(PORT, () => {
    console.log(`our web server is up on port: ${PORT}`);
})