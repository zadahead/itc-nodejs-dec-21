const express = require('express');
const route = express.Router();

//schema
const validator = require('../dto/validator');
const postUserSchema = require('../dto/post-user-schema');

//controllers
const userController = require('../controllers/users.controller');
const adminMiddleware = require('../middlewares/adminValidate');


//users
route.get('/', userController.getUsers);
route.get('/me', userController.getMe);
route.get('/admin', adminMiddleware, userController.getAdminData);
route.get('/:userId', userController.getById);

/// 
route.post('/', validator(postUserSchema), userController.addNew)
route.put('/:userId', userController.update);
route.patch('/:userId', userController.updateCol);
route.delete('/:userId', userController.del);


module.exports = route;