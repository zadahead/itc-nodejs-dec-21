const express = require('express');
const DB = require("../db");
const route = express.Router();

const authController = require('../controllers/auth.controller');





//schema
const validator = require('../dto/validator');
const authRegisterSchema = require('../dto/auth/auth.register.schema');
const authLoginSchema = require('../dto/auth/auth.login.schema');



route.post('/register', validator(authRegisterSchema), authController.register);
route.post('/login', validator(authLoginSchema), authController.login);

route.post('/refresh', (req, res, next) => {
    const refreshToken = req.headers.authorization;
    console.log('refreshToken', refreshToken);

    jwt.verify(refreshToken, sceretJwt, (err, decoded) => {
        if (err) {
            return next('you need to login')
        }

        const tokensDB = new DB('tokens');
        const pack = tokensDB.find((i) => i.refresh_token === refreshToken);
        if (!pack) { //milicios attemp
            return next('you need to login');
        }

        tokensDB.del(pack.id);

        const tokens = generateToken(pack.userId);

        res.send(tokens);
    });

});


//TODO: change location
const generateCookie = (res) => {
    const dataToSecure = "this is my great cookie";

    var oneWeek = 7 * 24 * 3600 * 1000; //1 weeks 

    res.cookie("secureCookie", JSON.stringify(dataToSecure), {
        secure: false,
        httpOnly: true,
        expires: new Date(Date.now() + oneWeek),
    });

}


module.exports = route;