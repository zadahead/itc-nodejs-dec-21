const express = require('express');
const DB = require("../db");
const route = express.Router();

//tweets
route.get('/', (req, res) => {
    console.log('GET tweets');

    const tweets = new DB('tweets');

    res.send(tweets.get())
})

route.get('/:tweetId', (req, res) => {
    const tweets = new DB('tweets');
    const user = tweets.getById(req.params.tweetId);

    res.send(user);
});

route.post('/', (req, res) => {

    const tweets = new DB('tweets');
    const newId = tweets.add(req.body);

    res.send(`TWEET Created1111 id: ${newId}`);
})

route.put('/:tweetId', (req, res) => {
    const id = req.params.tweetId;
    const item = req.body;

    const tweets = new DB('tweets');
    tweets.update(id, item);

    res.send('TWEET Updated');
})

route.patch('/:tweetId', (req, res) => {
    const id = req.params.tweetId;
    const data = req.body;

    const tweets = new DB('tweets');
    tweets.updateCol(id, data.key, data.value);

    res.send('TWEET Col Updated');
})

route.delete('/:tweetId', (req, res) => {
    const id = req.params.tweetId;

    const tweets = new DB('tweets');
    tweets.del(id);

    res.send('TWEET Deleted!');
})

module.exports = route;