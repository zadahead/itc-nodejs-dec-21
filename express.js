require('dotenv').config();
const express = require('express');
const cors = require('cors');
const authValidate = require('./middlewares/authValidate');
const app = express();
const PORT = process.env.API_PORT;

const cookieParser = require('cookie-parser');


//settings section
app.use(cookieParser());

app.use(cors({
    origin: 'http://localhost:3000',
    credentials: true
}))

app.use(express.json());
//app.use(express.urlencoded({ extended: true }));


app.use(authValidate);



app.use((req, res, next) => {
    console.log('middle 1');
    next()
})

/*
    res.status(status).json({
        error: true,
        message: message,
        statusCode: status
    })
*/




//routes
app.use('/users', require('./routes/users.route'));
app.use('/tweets', require('./routes/tweets'));
app.use('/auth', require('./routes/auth.route'));


//error handling
app.use((err, req, res, next) => {
    console.log(err);

    const { status, msg } = err;

    console.log('ERROR', err);
    res.status(status).send({
        error: true,
        message: msg
    });
})


//start listen
app.listen(PORT, () => {
    console.log(`our server is running on port: ${PORT}`);
})