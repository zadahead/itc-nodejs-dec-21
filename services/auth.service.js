

const DB = require('../db');
const hashLib = require('../lib/hash.lib');
const jwtLib = require('../lib/jwt.lib');

const generateHash = (password) => {
    const hash = hashLib.hash(password);
    return hash;
}

const validateHash = (password, hashedPassword) => {
    const hash = hashLib.check(password, hashedPassword);
    return hash;
}

const generateToken = (userId) => {
    const data = {
        userId
    }

    const access_token = jwtLib.sign(data);

    return {
        access_token
    }
}

//will be called from /login
const saveUserInCache = (user) => {
    const cache = new DB('redis');
    cache.add({
        userId: user.id,
        permissions: user.permissions
    })
}

module.exports = {
    generateHash,
    validateHash,
    generateToken,
    saveUserInCache
}