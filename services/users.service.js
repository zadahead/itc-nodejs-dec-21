const DB = require("../db");

const users = new DB('users');

const getAll = () => {
    return users.get();
}

const findByEmail = (email) => {
    return users.find((i) => i.email === email);
}

const getById = (id) => {
    return users.getById(id);
}

const add = (user) => {
    return users.add(user)
}

const update = (id, item) => {
    users.update(id, item)
}

const updateCol = (id, item) => {
    users.updateCol(id, item)
}

const del = (id, item) => {
    users.del(id, item)
}

module.exports = {
    getAll,
    findByEmail,
    getById,
    add,
    update,
    updateCol,
    del
}