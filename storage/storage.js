const express = require('express');
const cors = require('cors');
const PORT = 5000;

//**** Multer */

const multer = require('multer');

const storage = multer.diskStorage({
    destination: (req, file, cb) => {
        cb(null, 'uploads/');
    },
    filename: (req, file, cb) => {
        console.log(file);

        cb(null, 'asdasd---' + file.originalname);
    }
})

const upload = multer({ storage: storage });

/**** */

const app = express();

app.use(express.static(`${__dirname}/uploads`));

app.use(cors({
    origin: 'http://localhost:3000'
}))

app.use(express.json());


//
app.post('/upload', upload.single('image'), (req, res) => {
    res.send('uploaded');
})




app.listen(PORT, () => {
    console.log(`our storage server is running on port: ${PORT}`);
})